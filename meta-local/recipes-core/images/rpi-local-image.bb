# Base this image on core-image-minimal
include recipes-core/images/core-image-minimal.bb

# Include stuff in rootfs
IMAGE_INSTALL += " \
	binutils \
	hello \
	i2c-tools \
	programs \
	kernel-modules \
	timestamper \
	"
