LICENSE = "CLOSED"
LIC_FILE_CHKSUM = ""

SRC_URI = "git://bitbucket.org/muep/timestamper.git;protocol=https"
SRCREV = "31a1d0cc2b918d53665dc65a2c7b44106b018623"

S = "${WORKDIR}/git"

inherit cargo
