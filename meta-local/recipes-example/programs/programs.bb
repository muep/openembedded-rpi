LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "git://localhost/work/muep/openembedded/git/programs.git;protocol=ssh"
SRCREV = "efb916010c01f7f53597231e08d616903df2c1e3"

S = "${WORKDIR}/git"

inherit cmake
